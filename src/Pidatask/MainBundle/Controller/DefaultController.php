<?php

namespace Pidatask\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PidataskMainBundle:Default:index.html.twig', array('name' => 'World'));
    }
}
