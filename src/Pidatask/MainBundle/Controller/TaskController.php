<?php
namespace Pidatask\MainBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Pidatask\MainBundle\Entity\Task;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends FOSRestController
{
    public function getTasksAction(Request $request)
    {
        $taskRepo = $this->get('pidatask_main.repository.task');

        $data = $taskRepo->findAll();
        $view = $this
            ->view($data, 200)
            ->setFormat('json')
        ;

        return $this->handleView($view);
    }

    public function getTaskAction(Request $request, $id)
    {
        $taskRepo = $this->get('pidatask_main.repository.task');

        $data = $taskRepo->find($id);

        if (null === $data) {
            return $this->createNotFoundException('Task not found');
        }

        $view = $this
            ->view($data, 200)
            ->setFormat('json')
        ;

        return $this->handleView($view);
    }

    public function postTasksAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $task = new Task();
        $task->setTitle($request->request->get('title'));
        $task->setDescription('');

        $em->persist($task);
        $em->flush($task);

        $view = $this
            ->view($task, 200)
            ->setFormat('json')
        ;

        return $this->handleView($view);
    }

    public function putTasksAction(Request $request, $id)
    {
        $taskRepo = $this->get('pidatask_main.repository.task');
        $em = $this->get('doctrine.orm.entity_manager');

        $task = $taskRepo->find($id);
        if (null === $task) {
            throw $this->createNotFoundException('Task not found');
        }

        $task->setTitle($request->request->get('title'));
        $task->setDone((boolean) $request->request->get('done'));

        $em->merge($task);
        $em->flush();

        $view = $this
            ->view($task, 200)
            ->setFormat('json')
        ;

        return $this->handleView($view);
    }

    public function deleteTasksAction($id)
    {
        $taskRepo = $this->get('pidatask_main.repository.task');
        $em = $this->get('doctrine.orm.entity_manager');

        $task = $taskRepo->find($id);

        $em->remove($task);
        $em->flush();

        $view = $this
            ->view(null, 204)
            ->setFormat('json')
        ;

        return $this->handleView($view);
    }
}
