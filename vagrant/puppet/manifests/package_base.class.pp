
class package_base
{

  exec
  {
    'init':
      command => 'apt-get update',
      path    => '/usr/bin/'
  }

  package
  {
    'language-pack-en-base':
      ensure  => present,
      require => Exec['init']
  }
  
  exec
  {
  	 'permission export':
 	 command => 'chmod a+x /vagrant/vagrant/resources/app/exec/export.sh',
	 path => '/bin'
  }
  
  exec
  {
  	 'export':
 	 command => '/vagrant/vagrant/resources/app/exec/export.sh',
	 path => '/bin',
	 logoutput => "on_failure"
  }

  package
  {
    'htop':
      ensure  => present,
      require => Exec['init']
  }

}
